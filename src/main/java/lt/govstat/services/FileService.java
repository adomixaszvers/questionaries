package lt.govstat.services;

import lt.govstat.dao.FileDao;
import lt.govstat.domain.File;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class FileService {
    private FileDao fileDao;
    private Logger logger = Logger.getLogger(FileService.class);

    @Transactional
    public void persist(File file) {
        if (Objects.isNull(file)) {
            return;
        }
        logger.info(file.getName());
        fileDao.persist(file);
    }

    @Autowired
    public void setFileDao(FileDao fileDao) {
        this.fileDao = fileDao;
    }
}
