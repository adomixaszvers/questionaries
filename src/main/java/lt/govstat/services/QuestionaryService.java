package lt.govstat.services;

import lt.govstat.dao.QuestionaryDao;
import lt.govstat.domain.Questionary;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class QuestionaryService {
    private QuestionaryDao questionaryDao;
    private Logger logger = Logger.getLogger(QuestionaryService.class);

    @Transactional
    public void persist(Questionary questionary) {
        logger.info(questionary.getTitle());
        questionaryDao.persist(questionary);
    }

    @Autowired
    public void setQuestionaryDao(QuestionaryDao questionaryDao) {
        this.questionaryDao = questionaryDao;
    }
}
