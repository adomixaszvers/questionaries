package lt.govstat.utils;

import lt.govstat.domain.EmbeddedEntity;
import lt.govstat.domain.File;
import lt.govstat.domain.Questionary;
import lt.govstat.services.FileService;
import lt.govstat.services.QuestionaryService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.mail.internet.ContentDisposition;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scrapper {
    @Value("${questionaries.url}")
    private String questionariesUrl;

    @Value("${questionaries.url2}")
    private String questionariesUrl2;

    @Value("${questionaries.save_template}")
    private Boolean saveTemplate;

    private QuestionaryService questionaryService;
    private FileService fileService;

    public void scrap() throws IOException {
        Document document = Jsoup.connect(questionariesUrl).validateTLSCertificates(false).get();
        document
                .body()
                .select("table.records tbody tr")
                .parallelStream()
                .forEach(tr -> {
                    Questionary questionary = new Questionary();

                    EmbeddedEntity embeddedEntity = new EmbeddedEntity();

                    embeddedEntity.setCode(tr.child(0).text());
                    embeddedEntity.setPeriodicity(tr.child(1).text());

                    questionary.setEmbeddedEntity(embeddedEntity);

                    questionary.setTitle(tr.child(2).text());
                    questionary.setReportYear(Short.valueOf(tr.child(3).text()));
                    questionary.setPublishYear(Short.valueOf(tr.child(4).text()));
                    questionary.setVerificationDate(parseDateFromTd(tr.child(5)));
                    questionary.setOnline("Taip".equals(tr.child(6).text()));

                    if (saveTemplate) {
                        File template = parseFileFromTd(tr.child(7), embeddedEntity);
                        fileService.persist(template);
                    }

                    File form = parseFileFromTd(tr.child(8), embeddedEntity);
                    File help = parseFileFromTd(tr.child(9), embeddedEntity);
                    questionary.setReportTerm(tr.child(10).text());
                    questionary.setExecutivePerson(tr.child(11).text());
                    questionary.setPhoneNumber(tr.child(12).text());
                    questionary.setEmail(tr.child(13).text());

                    questionaryService.persist(questionary);
                    fileService.persist(form);
                    fileService.persist(help);
                });
        document = Jsoup.connect(questionariesUrl2).validateTLSCertificates(false).get();
        document
                .body()
                .select("ul.table-large li.row ul")
                .forEach(ul -> {
                    Questionary questionary = new Questionary();
                    questionary.setTitle(ul.select("li:nth-child(1) .value .cell").first().text());

                    EmbeddedEntity embeddedEntity =  new EmbeddedEntity();
                    String code = ul.select("li:nth-child(2) .value .cell").first().text();
                    embeddedEntity.setCode(code);
                    embeddedEntity.setPeriodicity("Nežinau :(");
                    questionary.setEmbeddedEntity(embeddedEntity);

                    File form = parseFileFromLi(ul.select("li:nth-child(2)").first(), embeddedEntity);
                    File otherData = parseFileFromLi(ul.select("li:nth-child(3)").first(), embeddedEntity);
                    parseOtherInfoFromLi(questionary, ul.select("li:nth-child(4)").first());

                    questionary.setVerificationDate(Date.from(Instant.EPOCH));
                    fileService.persist(form);
                    fileService.persist(otherData);
                    questionaryService.persist(questionary);
                });
    }

    private void parseOtherInfoFromLi(Questionary questionary, Element child) {
        String[] info = child.select(".value .cell").first().text().split(", ");
        questionary.setExecutivePerson(info[0]);
        questionary.setPhoneNumber(info[1]);
    }

    private File parseFileFromLi(Element li, EmbeddedEntity embeddedEntity) {
        String url = li.select(".value .cell a").first().absUrl("href");
        File file = new File();
        file.setUrl(url);
        file.setEmbeddedEntity(embeddedEntity);
        Connection.Response response = null;
        try {
            response = Jsoup
                    .connect(file.getUrl())
                    .ignoreContentType(true)
                    .validateTLSCertificates(false)
                    .execute();
            file.setData(response.bodyAsBytes());
            file.setMimeType(response.contentType());
            String fileName = new ContentDisposition(response.header("Content-Disposition"))
                    .getParameter("filename");
            file.setName(fileName);
            SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
            TimeZone gmtTime = TimeZone.getTimeZone("GMT");
            format.setTimeZone(gmtTime);
            Date lastModified = format.parse(response.header("Last-Modified"));
            file.setVersionDate(lastModified);
        } catch (IOException| javax.mail.internet.ParseException|ParseException e) {
            e.printStackTrace();
        }
        return file;
    }

    private Date parseDateFromTd(Element td) {
        String text = td.text();
        Pattern pattern = Pattern.compile("\\d{4}+.\\d{2}+.\\d{2}+", Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(text);
        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            matches.add(matcher.group(0));
        }
        if (matches.isEmpty()) {
            return Date.from(Instant.EPOCH);
        }
        String lastMatch = matches.get(matches.size() - 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(lastMatch);
        } catch (ParseException e) {
            return null;
        }
    }

    private File parseFileFromTd(Element td, EmbeddedEntity embeddedEntity) {
        Elements aElements = td.select("a");
        if (aElements.size() == 0) {
            return null;
        }
        String href = aElements.first().absUrl("href");
        File file = new File();
        file.setUrl(href);
        file.setVersionDate(parseDateFromTd(td));
        file.setEmbeddedEntity(embeddedEntity);
        getAllFileData(file);
        return file;
    }

    private void getAllFileData(File file) {
        try {
            if (Objects.isNull(file)) {
                return;
            }
            Connection.Response response = Jsoup
                    .connect(file.getUrl())
                    .ignoreContentType(true)
                    .validateTLSCertificates(false)
                    .execute();
            file.setData(response.bodyAsBytes());
            file.setMimeType(response.contentType());
            String fileName = new ContentDisposition(response.header("Content-Disposition"))
                    .getParameter("filename");
            file.setName(fileName);
        } catch (IOException | javax.mail.internet.ParseException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    public void setQuestionaryService(QuestionaryService questionaryService) {
        this.questionaryService = questionaryService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }
}
