package lt.govstat.dao;

import lt.govstat.domain.File;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FileDao {
    private SessionFactory sessionFactory;

    public void persist(File file) {
        Session session = sessionFactory.getCurrentSession();
        Boolean fileExists = !session
                .createCriteria(File.class)
                .add(Restrictions.and(
                        Restrictions.eq("embeddedEntity", file.getEmbeddedEntity()),
                        Restrictions.eq("versionDate", file.getVersionDate())
                ))
                .list()
                .isEmpty();
        if (!fileExists) {
            session.save(file);
        }
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
