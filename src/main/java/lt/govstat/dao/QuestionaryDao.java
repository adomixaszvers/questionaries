package lt.govstat.dao;

import lt.govstat.domain.Questionary;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class QuestionaryDao {
    private SessionFactory sessionFactory;

    public void persist(Questionary questionary) {
        Session session = sessionFactory.getCurrentSession();
        Boolean questionaryExists = !session
                .createCriteria(Questionary.class)
                .add(Restrictions.and(
                        Restrictions.eq("embeddedEntity", questionary.getEmbeddedEntity()),
                        Restrictions.eq("verificationDate", questionary.getVerificationDate())
                ))
                .list()
                .isEmpty();
        if (!questionaryExists) {
            session.save(questionary);
        }
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
