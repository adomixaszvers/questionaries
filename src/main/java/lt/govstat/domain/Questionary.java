package lt.govstat.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "questionaries")
public class Questionary {
    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    private EmbeddedEntity embeddedEntity;

    private String title;
    private Short reportYear;
    private Short publishYear;
    private Date verificationDate;
    private Boolean isOnline;
    private String reportTerm;
    private String executivePerson;
    private String phoneNumber;
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Short getReportYear() {
        return reportYear;
    }

    public void setReportYear(Short reportYear) {
        this.reportYear = reportYear;
    }

    public Short getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Short publishYear) {
        this.publishYear = publishYear;
    }

    public Date getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(Date verificationDate) {
        this.verificationDate = verificationDate;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public String getReportTerm() {
        return reportTerm;
    }

    public void setReportTerm(String reportTerm) {
        this.reportTerm = reportTerm;
    }

    public String getExecutivePerson() {
        return executivePerson;
    }

    public void setExecutivePerson(String executivePerson) {
        this.executivePerson = executivePerson;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EmbeddedEntity getEmbeddedEntity() {
        return embeddedEntity;
    }

    public void setEmbeddedEntity(EmbeddedEntity embeddedEntity) {
        this.embeddedEntity = embeddedEntity;
    }
}
