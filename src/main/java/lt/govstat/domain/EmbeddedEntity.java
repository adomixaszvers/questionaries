package lt.govstat.domain;

import javax.persistence.Embeddable;

@Embeddable
public class EmbeddedEntity {
    private String code;
    private String periodicity;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }
}
