package lt.govstat;

import lt.govstat.services.QuestionaryService;
import lt.govstat.utils.Scrapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class QuestionariesApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(QuestionariesApplication.class, args);
        Scrapper scrapper = ctx.getBean(Scrapper.class);
        QuestionaryService questionaryService = ctx.getBean(QuestionaryService.class);
        try {
            scrapper.scrap();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
