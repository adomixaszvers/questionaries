package lt.govstat.config;

import lt.govstat.utils.Scrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

@Configuration
public class ApplicationConfiguration {
    @Bean
    public Scrapper scrapper() {
        return new Scrapper();
    }

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }
}
